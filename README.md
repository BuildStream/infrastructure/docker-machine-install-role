docker-machine install
=========

Installs docker-machine, either by building from source or fetching an
appropriate release binary.

Role Variables
--------------

`docker_machine_url`: the URL to fetch the docker-machine source from.

`docker_machine_version`: the version of docker-machine to build. This can be a
branch, a tag, a SHA-1, or 'latest'.

`docker_machine_install_path`: the location docker-machine is installed to.

Example
-------

This role is not currently published to Ansible Galaxy, however since
`ansible-galaxy` supports installing from git you can create a
`requirements.yml` like so:

    ---
    - src: https://gitlab.com/buildstream/infrastructure/docker-machine-install-role.git
      scm: git
      name: docker-machine-install

After installing via `ansible-galaxy`, you can then include the role in your
playbook like so:

    ---
    - hosts: servers
      roles:
         - role: docker-machine-install
           docker_machine_version: v0.16.2
